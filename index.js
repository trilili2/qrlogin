var express = require("express");
var app = express();
var port = 3700;
 
//carrega o template e view
//app.set('views', __dirname + '/tpl');
//app.set('view engine', "jade");
//app.engine('jade', require('jade').__express);
app.get("/", function(req, res){
    res.sendFile( __dirname + '/public/index.html');
});
 
var io = require('socket.io').listen(app.listen(port));

io.sockets.on('connection', function (socket) {
    socket.emit('message', { message: 'Bem-vindo ao chat', socketid: socket.id });
    socket.on('send', function (data) {
        console.log(data.usuario);
//        io.sockets.emit('message', data);
        if (data.usuario.length != 0) {
            if (io.sockets.connected[data.usuario]) {
                io.sockets.connected[data.usuario].emit('message', data );
            }
        }
        else {
            io.sockets.emit('message', data);
        }


    });
});

//mensagem que aparece no servidor
console.log("Servidor inicializado");
console.log("Listening on port " + port);