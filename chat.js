window.onload = function() {
 
    var messages = [];
    var socket = io.connect('http://10.4.1.195:3700');
    var field = document.getElementById("field");
    var sendButton = document.getElementById("send");
    var content = document.getElementById("content");
    var nicknames = document.getElementById("nicknames");
    var name = document.getElementById("name");
    var id = "";
    var nicks = [];
    var socketid = '';
    var urlid = '';

    var d = new Date();
    var n = d.getHours()+''+d.getMinutes()+''+d.getSeconds();
    document.getElementById("name").setAttribute("name", n);

    socket.on('message', function (data) {

        urlid = getUrlParameter('urlid');
        if (urlid) {
            socket.emit('send', { message: "Autenticado", username: 'Auth', id: socketid, usuario: urlid });
        }
        else {
            if(data.message) {
                messages.push(data);
                var html = '';
                var nick = '';

                for(var i=0; i<messages.length; i++) {
                    html += '<b>' + (messages[i].username ? messages[i].username : 'Admin') + ': </b>';
                    html += messages[i].message + '<br />';
                    nicks[messages[i].id] = messages[i].username;
                    if (socketid.length == 0)
                    {
                        socketid = messages[i].socketid;
                        showImage(socketid);

                    }
                }
                content.innerHTML = html;

                var usuarios_unicos = [];

                for (var key in nicks) {
                    if (key != 'undefined') {
                        nick += nicks[key]+'<br />';
                        usuarios_unicos[key] = nicks[key];
                    }
                }

                if (nick.length !== 0) {
                    nicknames.innerHTML = nick;

                    var usuario = $('#usuarios');
                    usuario.find('option').remove().end().append($("<option />").val("").text('TODOS'));
                    for (var usu in usuarios_unicos) {
                        usuario.append($("<option />").val(usu).text(usuarios_unicos[usu]));
                    }
                }

            } else {
                console.log("There is a problem:", data);
            }

        }


    });
 
    sendButton.onclick = function() {
        if(name.value == "") {
            alert("Please type your name!");
        } else {
            var text = field.value;
            socket.emit('send', { message: text, username: name.value, id: socketid, usuario: $('#usuarios').val() });
        }
    };


	field.onkeyup = function(e) {
		if (e.keyCode == 13) {
            sendButton.click();
		}
	};





}

function showImage(id) {
    var img = $('<img id="dynamic">'); //Equivalent: $(document.createElement('img'))
    img.attr('src', 'http://api.qrserver.com/v1/create-qr-code/?color=000000&bgcolor=FFFFFF&data=http%3A%2F%2F10.4.1.195%3A3700%2F%3Furlid%3D'+id+'&qzone=1&margin=0&size=400x400&ecc=L');
    img.appendTo('#imagediv');
}

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
        else {
            return false;
        }
    }
}
